#!/bin/bash
lang=$1
file=$2
if [[ -n "$lang" ]]; then
  while read -r line
  do
    mod_name=$(basename "${file%/*/*}")
    path_name=${file%/*}
    if [[ "$line" =~ ^# ]]; then
      echo $line
      echo $line >> "$path_name/$mod_name.$lang.tr"
    else
      echo -e "${line%?}"
      new_line=$(trans -b -no-ansi -no-warn -s en -t $lang "${line%?}")
      echo -e "\033[0;32m${new_line}\033[0;0m"
      echo $line$new_line >> "$path_name/$mod_name.$lang.tr"
    fi
  done < $file
else
  echo "👊 No language specified. Aborting."
fi
